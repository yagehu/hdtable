#!/bin/sh

rm -rf doc/source/api
sphinx-apidoc -o doc/source/api --no-toc -f -e hdtable
sed -i '1s/.*/API Documentation/' doc/source/api/hdtable.rst
sed -i '2s/.*/=================/' doc/source/api/hdtable.rst
cd doc && make html
