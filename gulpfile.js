var execSync = require("child_process").execSync;
var gulp = require("gulp");

var watcher = gulp.watch("**/*.py", ["default"]);

gulp.task("default", function() {
    try {
        execSync("mypy -p hdtable -p tests --strict --ignore-missing-imports", {
            stdio: [0, 1, 2]
        });
        execSync("python -m unittest -v", { stdio: [0, 1, 2] });
    } catch (e) {}
});

watcher.on("change", function(event) {});
