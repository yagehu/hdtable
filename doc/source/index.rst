.. HDTable documentation master file, created by
   sphinx-quickstart on Mon Nov 12 16:31:03 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HDTable's documentation!
===================================

.. toctree::
    :maxdepth: 4
    :caption: Contents:

    api/hdtable

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
